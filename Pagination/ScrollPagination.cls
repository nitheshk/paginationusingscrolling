public with sharing class ScrollPagination {
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public list<contactWrapper> contWpr{get;set;}
    public final Integer Page_Size=10;
    public set<id> selectedContactIds{ get;private set;}
    
    public ScrollPagination()
    {
       selectedContactIds=new  set<id>();
    }
    
    
    public class contactWrapper{
        public Contact cont {get;set;}
        public Boolean isSelected{get;set;}
        public contactWrapper(contact c,Boolean s)
        {
            cont=c;
            isSelected=s;
        }
    }
    
    public void contactSelection()
    {
        Id id=(Id)ApexPages.currentPage().getParameters().get('cId');
        if(selectedContactIds.contains(id))
            selectedContactIds.remove(id);
            else
            selectedContactIds.add(id);
    }
    
    public void deleteContacts()
    {
        List<contact> contactToDelete=[select id from contact where id in :selectedContactIds];
        if(contactToDelete.size()!=0)  {
               try { delete contactToDelete; }
               catch(exception ex) { System.debug(ex); }
                refresh();
           }
          
    }
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size=Page_Size;
                string queryString = 'Select Id,Name, Email, Birthdate, Phone, MobilePhone from Contact order by Name';
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
     
      Public list<contactWrapper> getContacts(){
         contWpr =new list<contactWrapper>();
        for(Contact c: (List<Contact>)setCon.getRecords())
            if(selectedContactIds.contains(c.id))
                contWpr.add(new contactWrapper(c,true));
                else
                 contWpr.add(new contactWrapper(c,false));
        return contWpr;
    }
     

     
    public pageReference refresh() {
        setCon = null;
        selectedContactIds=new set<id>();
        getContacts();
        setCon.setPageNumber(1);
        return null;
    }
     
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    
     public void next() {
        setCon.next();
            // do you operation here 
    }
    
       public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
    public void skip(){}
  
  /* Additional Functionality you can perform on StandardSetController, 
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
 
    public void first() {
        setCon.first();
    }
  
    public void last() {
        setCon.last();
    }
  
    public void previous() {
        setCon.previous();
    }
  */
   
}